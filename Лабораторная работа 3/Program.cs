﻿using System;

namespace Лабораторная_работа_3
{
    class Program
    {
        static void Main(string[] args)
        {
            double x, y;
            Console.WriteLine($"Введите занчение х: ");
            x = Convert.ToDouble(Console.ReadLine());
            if (x > -2.5)
            {
                y = 3 * x - 2;
            }
            else if (-10 <= x && x <= -2.5)
            {
                y = 4 + x;
            }
            else
            {
                y = 12 * x;
            }
            Console.WriteLine($"Полученное значение y: {y} ");
        }
    }
}
